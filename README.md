# To setup template:
* Change dir to template `cd template`
* Install Node.js dependencies with `npm run build`
* Start dev `npm run dev`
* Build prod `npm run prod`