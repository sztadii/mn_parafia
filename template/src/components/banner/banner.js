var banner;

banner = new function () {

  //catch DOM
  var $el;
  var $slider;
  var $sliderHeader;

  //bind events
  $(document).ready(function () {
    init();
  });

  $(window).scroll(function () {
    animation.fading($el.find('.container'));
  });

  //private functions
  var init = function () {
    $el = $('.banner.-main');
    $slider = $el.find('.banner__slider');
    $sliderHeader = $slider.find('.banner__header');

    if ($slider.length > 0) {
      $slider.imagesLoaded({background: true}).always(function () {

        $slider.slick({
          infinite: true,
          dots: false,
          arrows: true,
          appendArrows: $el.find('.banner__arrows'),
          nextArrow: '<img class="banner__arrow -next" src="resources/img/banner__arrow-next.png" alt="arrow">',
          prevArrow: '<img class="banner__arrow -prev" src="resources/img/banner__arrow-prev.png" alt="arrow">',
          slidesToShow: 1,
          slidesToScroll: 1,
          fade: true,
          adaptiveHeight: true,
          autoplay: true,
          speed: 500,
          autoplaySpeed: 3000,
          draggable: false,
          pauseOnHover: true,
          pauseOnFocus: true
        });

        $slider.on('beforeChange', function () {
          $sliderHeader.addClass('-hide');
        });

        $slider.on('afterChange', function () {
          $sliderHeader.removeClass('-hide');
        });
      });
    }
  };
};
