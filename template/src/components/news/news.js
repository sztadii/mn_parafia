var news;

news = new function () {

  //catch DOM
  var $el;
  var $slider;

  //bind events
  $(document).ready(function () {
    init();
  });

  //private functions
  var init = function () {
    $el = $(".news");
    $slider = $el.find('.news__slider');

    $el.imagesLoaded({background: true}).always(function () {
      $slider.slick({
        arrows: false,
        dots: true,
        dotsClass: 'slick__dots',
        customPaging: function () {
          return '<div class="slick__dot"></div>'
        },
        appendDots: $('.news'),
        slidesToShow: 5,
        slidesToScroll: 5,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 4000,
        vertical: true,
        cssEase: 'linear',
        draggable: false,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 3
            }
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2
            }
          }
        ]
      });
    });
  };
};