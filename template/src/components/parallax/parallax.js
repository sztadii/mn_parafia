var parallax;

parallax = new function () {

  //private vars
  var breakpoint = 1024;

  //catch DOM
  var objects;
  var body;

  //bind events
  $(document).ready(function () {
    init();
    start();
  });

  $(window).bind("scroll resize", function () {
    start();
  });

  //private functions
  var init = function () {
    body = $('body');
    objects = [].slice.call(document.querySelectorAll(".parallax"));
  };

  var start = function () {
    if ($(window).width() >= breakpoint) {
      var object, objectTop, objectHeight;
      var windowTop = getWindowOffset();
      var windowHeight = getWindowHeight();

      for (var i in objects) {
        object = objects[i];
        objectTop = object.offsetTop;
        objectHeight = object.offsetHeight;

        objectTop > windowTop + windowHeight ||
        windowTop > objectTop + objectHeight ||
        (object.style.backgroundPosition = "50% " + Math.round(2 * (objectTop - windowTop) / 4) + "px");
      }
    }
  };

  var getWindowHeight = function () {
    var a = document.documentElement.clientHeight, b = window.innerHeight;
    return b > a ? b : a
  };

  var getWindowOffset = function () {
    if ("undefined" != typeof window.scrollY) return window.scrollY;
    if ("undefined" != typeof pageYOffset) return pageYOffset;
    var a = document.documentElement;
    return a = a.clientHeight ? a : document.body, a.scrollTop
  };
};